import React from 'react';
import {
  Router, Route, Switch, Redirect,
} from 'react-router-dom';
import { connect } from 'react-redux';

import {
  Button, Modal, Form, Input, Radio,
} from 'antd';
import Select from 'react-select';


import { history } from '../_helpers';
import { alertActions } from '../_actions';
import { PrivateRoute } from '../_components';
import { HomePage } from '../HomePage';
import { LoginPage } from '../LoginPage';
import { RegisterPage } from '../RegisterPage';
import { CardsPage } from '../CardsPage';

import './App.css';
import NewsPage from '../NewsPage/NewsPage';
import ShopsPage from '../ShopsPage/ShopsPage';


class App extends React.Component {
  constructor(props) {
    super(props);

    history.listen((location, action) => {
      // clear alert on location change
      this.props.clearAlerts();
    });
  }

  componentDidMount() {
    const mainNav = document.getElementById('js-menu');
    const navBarToggle = document.getElementById('js-navbar-toggle');

    navBarToggle.addEventListener('click', () => {
      mainNav.classList.toggle('active');
    });
  }


  render() {
    const { alert, authentication: { loggedIn } } = this.props;
    return (
      <div>
        <nav className="navbar-custom">
          <span className="navbar-toggle-custom" id="js-navbar-toggle">
            <i className="fa fa-bars" />
          </span>
          <a href="/" className="logo-custom  ">Discount Cards Minsk</a>
          <ul className="main-nav" id="js-menu">
            <li>
              <a href="/cards" className="nav-links">Cards</a>
            </li>

            <li>
              <a href="/news" className="nav-links">News</a>
            </li>

            <li>
              <a href="/shops" className="nav-links">Shops</a>
            </li>

            <li>
              <a href="/login" className="nav-links">{loggedIn ? 'Logout' : 'Login'}</a>
            </li>
          </ul>
        </nav>


        <div className="jumbotron">
          <div className="container">
            <div className="col-sm-8 col-sm-offset-2">
              {alert.message && <div className={`alert ${alert.type}`}>{alert.message}</div>}
              <Router history={history}>
                <Switch>
                  <PrivateRoute exact path="/" component={HomePage} />
                  <Route path="/login" component={LoginPage} />
                  <Route path="/cards" component={CardsPage} />
                  <Route path="/register" component={RegisterPage} />
                  <Route path="/news" component={NewsPage} />
                  <Route path="/shops" component={ShopsPage} />
                  <Redirect from="*" to="/" />
                </Switch>
              </Router>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  alert: state.alert,
  authentication: state.authentication,
});

const actionCreators = {
  clearAlerts: alertActions.clear,
};

const connectedApp = connect(mapStateToProps, actionCreators)(App);
export { connectedApp as App };
