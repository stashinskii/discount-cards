import {
  Modal, Form, Input,
} from 'antd';
import React from 'react';

const AddShopModal = Form.create({ name: 'add-article-modal' })(
  // eslint-disable-next-line
  class extends React.Component {
    onSubmit = () => {
      const { onCreate, form: { validateFields } } = this.props;
      validateFields((err, values) => {
        if (err) { return; }
        onCreate(values);
      });
    }

    render() {
      const {
        visible, onCancel, form,
      } = this.props;
      const { getFieldDecorator } = form;
      return (
        <Modal
          visible={visible}
          title="Add new shop"
          okText="Create"
          onCancel={onCancel}
          onOk={this.onSubmit}
        >
          <Form layout="vertical">
            <Form.Item label="Shop Name">
              {getFieldDecorator('shopName', {
                rules: [{ required: true, message: 'Please input shop name!' }],
              })(<Input />)}
            </Form.Item>
            <Form.Item label="URL">
              {getFieldDecorator('shopWebSiteUrl', {
                rules: [{ required: true, message: 'Please input shop website url!', type: 'url' }],
              })(<Input />)}
            </Form.Item>
            <Form.Item label="Category">
              {getFieldDecorator('shopCategory', {
                rules: [{ required: true, message: 'Please input category!' }],
              })(<Input />)}
            </Form.Item>
            <Form.Item label="Description">
              {getFieldDecorator('shopDescription', {
                rules: [{ required: true, message: 'Please input description!' }],
              })(<Input />)}
            </Form.Item>
            <Form.Item label="Image URL">
              {getFieldDecorator('imageUrl', {
                rules: [{ required: true, message: 'Please input valid image URL!', type: 'url' }],
              })(<Input />)}
            </Form.Item>
          </Form>
        </Modal>
      );
    }
  },
);

export default AddShopModal;
