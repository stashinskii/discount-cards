import React from 'react';
import { connect } from 'react-redux';
import {
  Button, Modal, Form, InputNumber,
} from 'antd';
import 'antd/dist/antd.css';
import { newsActions, shopsActions } from '../_actions';
import './ShopsPage.sass';
import AddShopModal from './AddShopModal/AddShopModal';

class ShopsPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      formVisible: false,
    };
  }

  componentDidMount() {
    const { getAll } = this.props;
    getAll();
  }

  handleFormCancel = () => {
    this.setState({ formVisible: false });
  };

  handleSubmit = (shop) => {
    const { addShop } = this.props;
    addShop(JSON.stringify(shop));
    this.setState({ formVisible: false });
  };

  openForm = () => {
    this.setState({
      formVisible: true,
    });
  }

  showCardsModal = (shop) => {
    this.setState({
      cardsModalVisible: true,
      currentShop: shop,
    });
  }

  deleteShop = (shopId) => {
    const { deleteShop } = this.props;
    deleteShop(shopId);
  }

  render() {
    const { shops, user } = this.props;
    const { formVisible, cardsModalVisible, currentShop } = this.state;

    return (
      <div className="shops">
        {user && user.isAdmin && (
          <>
            <Button type="success" className="admin-create-card-form" onClick={this.openForm}>
              Add shop
            </Button>
            <AddShopModal
              wrappedComponentRef={this.saveFormRef}
              visible={formVisible}
              onCancel={this.handleFormCancel}
              onCreate={this.handleSubmit}
            />
          </>
        )}
        {shops.map((shop) => (
          <div className="card shop" key={shop.shopId}>
            <div className="card-body">
              <h5 className="card-title">
                <a href={shop.shopWebSiteUrl}>
                  {shop.shopName}
                </a>
              </h5>
              <div className="card-text">
                {shop.shopDescription}
              </div>
              <div className="category">
                <span className="label label-info">
                  {shop.shopCategory}
                </span>

              </div>
                <span>
                  <a
                    onClick={() => this.showCardsModal(shop)}
                    className="btn btn-dark"
                  >
                    Cards
                  </a>
                </span>
              {user && user.isAdmin && (
                <span>
                  <a
                    onClick={() => this.deleteShop(shop.shopId)}
                    className="btn btn-dark"
                  >
                    Delete shop 🔑
                  </a>
                </span>
              )}
            </div>
            <img src={shop.imageUrl} className="image" alt="shop" />
          </div>
        ))}
        <Modal
          title="Available cards"
          visible={cardsModalVisible}
          onOk={() => this.setState({ cardsModalVisible: false })}
          onCancel={() => this.setState({ cardsModalVisible: false })}
          state={this.state}
        >
          {currentShop && currentShop.availibleCards.map((card, index) => (
            <div className="card" key={index} style={{ marginBottom: `${20}px` }}>
              <div className="card-body" style={{ padding: `${20}px` }}>
                <p>
                  <b>Discount info:</b>
                  {' '}
                  {card.discountInfo}
                </p>
                <p>
                  <b>Discount percent:</b>
                  {' '}
                  {card.discountPercent}
                  %
                </p>
              </div>
            </div>
          ))}
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  shops: state.shops.shopsItems,
  user: state.authentication.user,
});

const mapDispatchToProps = (dispatch) => ({
  getAll: () => dispatch(shopsActions.getAllShops()),
  addShop: (shop) => dispatch(shopsActions.addShop(shop)),
  deleteShop: (shopId) => dispatch(shopsActions.deleteShop(shopId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ShopsPage);
