import config from 'config';
import { authHeader } from '../_helpers';


function getShops() {
  const requestOptions = {
    method: 'GET',
    headers: authHeader(),
  };

  return fetch(`${config.apiUrl}/Shops/GetShops`, requestOptions)
    .then((response) => response.json())
    .then((data) => data.shops);
}


function addShop(shop) {
  const dict = authHeader();
  dict['Content-Type'] = 'application/json-patch+json';

  const requestOptions = {
    method: 'POST',
    headers: dict,
    body: shop,
  };

  return fetch(`${config.apiUrl}/Shops/AddShop`, requestOptions)
    .then((response) => {
      response.json();
    })
    .catch((e) => console.log(e))
    .then((data) => data);
}

function deleteShop(shopId) {
  const dict = authHeader();

  const requestOptions = {
    method: 'DELETE',
    headers: dict,
  };

  return fetch(`${config.apiUrl}/Shops/DeleteShop/${shopId}`, requestOptions)
    .then((response) => {
      response.json();
    })
    .catch((e) => console.log(e))
    .then((data) => data);
}

export const shopsService = {
  getShops,
  addShop,
  deleteShop,
};

export default shopsService;
