import config from 'config';
import { authHeader } from '../_helpers';

function getCards() {
  const requestOptions = {
    method: 'GET',
    headers: authHeader(),
  };

  return fetch(`${config.apiUrl}/DiscountCards/GetAllCards`, requestOptions)
    .then((response) => response.json())
    .then((data) => data.discountCards);
}

function issueCard(cardId) {
  const requestOptions = {
    method: 'POST',
    headers: authHeader(),
  };

  return fetch(`${config.apiUrl}/DiscountCards/IssueCard/${cardId}`, requestOptions)
    .then((response) => {
      response.json();
    })
    .catch((e) => alert(e))
    .then((data) => data);
}

function addNewCard(card) {
  const dict = authHeader();
  dict['Content-Type'] = 'application/json-patch+json';

  const requestOptions = {
    method: 'POST',
    headers: dict,
    body: card,
  };

  return fetch(`${config.apiUrl}/DiscountCards/AddNewCard`, requestOptions)
    .then((response) => {
      response.json();
    })
    .catch((e) => alert(e))
    .then((data) => data);
}

function updateCard(card) {
  const dict = authHeader();
  dict['Content-Type'] = 'application/json-patch+json';

  const requestOptions = {
    method: 'PUT',
    headers: dict,
    body: card,
  };

  return fetch(`${config.apiUrl}/DiscountCards/UpdateCard`, requestOptions)
    .then((response) => {
      response.json();
    })
    .catch((e) => alert(e))
    .then((data) => data);
}

function verifySignature(params) {
  const dict = authHeader();
  dict['Content-Type'] = 'application/json-patch+json';

  const requestOptions = {
    method: 'POST',
    headers: dict,
    body: params,
  };

  return fetch(`${config.apiUrl}/DiscountCards/VerifySignature`, requestOptions)
    .then((response) => {
      console.log(response);
      response.json();
    })
    .catch((e) => alert(e))
    .then((data) => data);
}

function deleteCardAdmin(cardId) {
  const dict = authHeader();

  const requestOptions = {
    method: 'DELETE',
    headers: dict,
  };

  return fetch(`${config.apiUrl}/DiscountCards/DeleteCard/${cardId}`, requestOptions)
    .then((response) => {
      response.json();
    })
    .catch((e) => alert(e))
    .then((data) => data);
}

function removeCard(cardId) {
  const dict = authHeader();

  const requestOptions = {
    method: 'DELETE',
    headers: dict,
  };

  return fetch(`${config.apiUrl}/DiscountCards/RemoveCard/${cardId}`, requestOptions)
    .then((response) => {
      response.json();
    })
    .catch((e) => alert(e))
    .then((data) => data);
}

export const cardsService = {
  getCards,
  issueCard,
  addNewCard,
  deleteCardAdmin,
  removeCard,
  updateCard,
  verifySignature,
};

export default cardsService;
