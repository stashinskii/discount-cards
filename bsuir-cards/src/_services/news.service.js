// TODO update backend to fit from and to
import config from 'config';
import { authHeader } from '../_helpers';

function handleResponse(response) {
  return response.articles;
}

function getNews() {
  const requestOptions = {
    method: 'GET',
  };

  return fetch(`${config.apiUrl}/Article/GetArticles`, requestOptions)
    .then((response) => response.json()).then(handleResponse);
}

function addArticle(article) {
  const dict = authHeader();
  dict['Content-Type'] = 'application/json-patch+json';

  const requestOptions = {
    method: 'POST',
    headers: dict,
    body: article,
  };

  return fetch(`${config.apiUrl}/Article/AddArticle`, requestOptions)
    .then((response) => {
      response.json();
    })
    .catch((e) => alert(e))
    .then((data) => data);
}

function deleteArticle(articleId) {
  const dict = authHeader();

  const requestOptions = {
    method: 'DELETE',
    headers: dict,
  };

  return fetch(`${config.apiUrl}/Article/Delete/${articleId}`, requestOptions)
    .then((response) => {
      response.json();
    })
    .catch((e) => alert(e))
    .then((data) => data);
}


export const newsService = {
  getNews,
  addArticle,
  deleteArticle
};

export default newsService;
