export const newsConstants = {
  GET_NEWS_REQUEST: 'GET_NEWS_REQUEST',
  GET_NEWS_REQUEST_SUCCESS: 'GET_NEWS_REQUEST_SUCCESS',
};

export default newsConstants;
