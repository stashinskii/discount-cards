import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { userActions } from '../_actions';

class LoginPage extends React.Component {
  constructor(props) {
    super(props);

    // reset login status
    this.props.logout();

    this.state = {
      email: '',
      password: '',
      submitted: false,
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  }

  handleSubmit(e) {
    e.preventDefault();

    this.setState({ submitted: true });
    const { email, password } = this.state;
    if (email && password) {
      this.props.login(email, password);
    }
  }

  render() {
    const { loggingIn } = this.props;
    const { email, password, submitted } = this.state;
    return (
      <div className="col-md-6 col-md-offset-3">
        <h2>Login</h2>
        <form name="form" onSubmit={this.handleSubmit}>
          <div className={`form-group${submitted && !email ? ' has-error' : ''}`}>
            <label htmlFor="email">Email</label>
            <input type="text" className="form-control" name="email" value={email} onChange={this.handleChange} />
            {submitted && !email
                            && <div className="help-block">email is required</div>}
          </div>
          <div className={`form-group${submitted && !password ? ' has-error' : ''}`}>
            <label htmlFor="password">Password</label>
            <input type="password" className="form-control" name="password" value={password} onChange={this.handleChange} />
            {submitted && !password
                            && <div className="help-block">Password is required</div>}
          </div>
          <div className="form-group">
            <button className="btn btn-primary" type="submit">Login</button>
            {loggingIn && (
            <img
              alt="spinner"
              className="spinner"
              src="data:image/gif;base64,R0lGODlhEAAQAIQAAAQCBISGhExKTNTW1KyqrGRmZBweHJSSlOTm5GxubAwKDIyKjFxaXNze3Kyu
                rGxqbJyanOzu7HRydAwODP///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C05F
                VFNDQVBFMi4wAwEAAAAh+QQJAwAUACwAAAAAEAAQAAAFMCAlUtHBOGOaHhMAGKpauG4SjwtdDLeI
                KC5CbxSoDUcCV+MogkxszKh0Sq1ar1hVCAAh+QQJAwAUACwAAAAAEAAQAIQEAgSEhoRERkTc2tws
                LiwMDgysrqyMjoxkYmT8+vwMCgwEBgSMioxMTkzs6uw0MjQUEhSUkpRsbmz8/vz///8AAAAAAAAA
                AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFMSAlUs7REE80rtTEFEAMFKw4ITKwQEdNMTLJwGdT
                xAzElQAQSLISzqh0Sq1ar9isNAQAIfkECQMAFgAsAAAAABAAEACEBAIEjIqMTEpM1NLULCosxMLE
                bG5s7OrslJaUPDo8DAoMVFZUNDI0/P78lJKUTE5M3NrcLC4sfH589PL0nJ6cDA4M////AAAAAAAA
                AAAAAAAAAAAAAAAAAAAAAAAAAAAABTCgJVpHIBDRg0zj2AQKIM9MK0ozUMVAZFsLmQEiGlAOv1Lh
                x2w6n9CodEqtWq/YaAgAIfkECQMAFwAsAAAAABAAEACEBAIEjIqM1NbUREZEHB4c7O7svLq8nJqc
                5ObkZGZkJCYkDA4M/P78lJKU3N7cTEpMJCIk9PL0nJ6c7OrsbGpsLCosFBIU////AAAAAAAAAAAA
                AAAAAAAAAAAAAAAAAAAABTLgJV5TMCjV00xjy0BALC9HKzJEvCwy0NgXhMQhEiRiFKDS0Igon9Co
                dEqtWq/YrBYaAgAh+QQJAwAWACwAAAAAEAAQAIQEAgSEgoRMSkzc2twcGhxkZmSUlpQMDgz8+vx8
                enwkJiSkoqQEBgSMjoxcXlzs6uwcHhxsbmycmpwUEhT8/vx8fnz///8AAAAAAAAAAAAAAAAAAAAA
                AAAAAAAAAAAAAAAFL6AljpWiCM0zriICvO9hsGs1HTAQ0exQwBIeS/KCCFkJgOPIojCf0Kh0Sq1a
                r5YQACH5BAkDABIALAAAAAAQABAAhAQCBIyOjNTS1ExOTKSmpBQSFJSWlOzq7FxeXGRmZAwODJSS
                lNza3KyurBwaHJyanOzu7GxqbP///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
                AAAAAAUwoCSO4jMEB6mOCgAowaomrpvIJEO7Bk4SLgXEN9otiCKBC4EUJRyNpnRKrVqvWFIIACH5
                BAkDABAALAAAAAAQABAAhAQCBISGhERGRNTW1DQyNAwODGRmZOzu7KSmpPz6/AwKDJSSlFRSVBQS
                FHx+fPz+/P///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAUv
                ICSOZGmWQrOcZQK8Bks6L7DKIvIWBy4arxtu8GL4IC4A4QgpKJkBhZBJrVqv2BAAIfkECQMAFQAs
                AAAAABAAEACEBAIEhIKE1NLUTE5MLCosvL68lJKU7OrsbGpsDAoMVFZUNDY0nJqc/P78jI6M1NbU
                VFJULC4slJaU9PL0DA4M////AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABTJgJY5kaZ7o
                eShBSjoA4LhiEScHXSHxTD+xga6SAER0jeKRJgjqJDGD7kBYTIbYrNYUAgAh+QQJAwAWACwAAAAA
                EAAQAIQEAgSMjoxMSkzU1tQcHhzs7uy8uryUlpRkYmQsKiwMDgzk5uT8/vyUkpRMTkzc2twkJiT0
                8vScmpxkZmQsLiwUEhT///8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFMaAljmRpnmiqmgyz
                igsBFa8EAME73MJrKQCID0jR8V6IW05luCloqsPt8CpMGr7sKQQAIfkECQMAEwAsAAAAABAAEACE
                BAIEhIKETEpM1NbUHB4clJaUZGZkDA4M7O7spKKkfHp8BAYEjI6MXFpcLCosnJqcbG5sFBYU/Pr8
                ////AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABS7gJI5kaZ5oqq5sO0pusARt
                BDjtcbMDAAgsg4+xSvgOCJXCByioJEwIqxFxukghACH5BAkDABIALAAAAAAQABAAhAQCBIyOjNTS
                1FRSVJyenBQSFOzq7GRmZJSWlKSmpAwODJSSlNza3FxaXBwaHOzu7GxqbKyqrP///wAAAAAAAAAA
                AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAUsoCSOZGmeaKqubOu+cAkpBMsAwMAeeLAm
                OIUhJeD1UhAc7qByBBe/weJBCgEAIfkECQMAEAAsAAAAABAAEACEBAIEhIaETE5M1NbULC4sDA4M
                rKqsbGpsjI6M7OrsDAoMZGZkFBIUfH58lJKU/Pr8////AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
                AAAAAAAAAAAAAAAAAAAAAAAAAAAABS0gJI5kaZ5oqq5s675wTD5uAwisAQCMOhw7AOIUYCiCgAXq
                GFQMUQgCQYBIkEIAIfkECQMAEQAsAAAAABAAEACEBAIEhIKExMLETEpM3NrcLCoslJKUXF5c7Ors
                DA4MjI6M1NbUTE5MNDI0nJ6cbGps7O7s////AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
                AAAAAAAAAAAAAAAABS5gJI5kaZ5oqq5s675wLMOCAZmIs4jEAwAHU+GXSPx+AeHxmFCcIApGoTBQ
                IEghACH5BAkDABMALAAAAAAQABAAhAQCBIyKjNza3ExKTCQiJLy+vJSWlOzq7AwODJSSlGRmZCwq
                LIyOjOTm5CQmJJyenPTy9BQSFGxqbP///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
                AAAAAAUv4CSOZGmeaKqubOu+cCyfkFG0CgAogig8DVNAp0NEdIRTAkEkOlCHwMDhGAQOpBAAIfkE
                CQMAEwAsAAAAABAAEACEBAIEhIKEREZE3NrcHBocpKakZGZkDA4MjI6M/P78fH58BAYEjIqMXFpc
                7OrsJCYkbG5sFBIUlJKU////AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABS3g
                JI5kaZ5oqq5s676wmrgNoLQEABQsogOQwcrwA0QIgRTiUATMUI6A4PG4lUIAIfkECQMAEgAsAAAA
                ABAAEACEBAIEjIqMTE5M1NbUZGJkFBIUnJ6cbG5sDAoM7OrsjI6MVFZU3N7cbGpsHBocrKqsdHJ0
                DA4M////AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABSygJI5kaZ5oqq5s
                677o4xztAgADq9wNmyC3h+4GODBWBGJxFQACCr6AwIAKAQAh+QQJAwASACwAAAAAEAAQAIQEAgSE
                hoRMSkzc2twsKiwMDgysrqz8+vwMCgyMjoxkYmQEBgSMiozs6uwsLiwUFhT8/vxsbmz///8AAAAA
                AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFLqAkjmRpnmiqrmxbJkjSOsDTEkDR
                CgAwsIxehNVY9AzAHiCwgiiUh9UhUBC4WCEAIfkECQMAFwAsAAAAABAAEACEBAIEjIqMREZExMbE
                JCYk5ObkdHJ0DA4MlJaU/P78XFpc1NbULC4s7O7sDAoMjI6MTEpM7OrsfH58FBIUnJ6c3N7cNDI0
                ////AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABTHgJY5kaZ5oqqaNxUTr9QAAFUP0EhPAEV88X0xA
                q8QCNEOs4KANjjRELCFRNH7Y7CUEACH5BAkDABIALAAAAAAQABAAhAQCBISGhNza3ERGRLy+vCQi
                JJyanPTy9AwODIyOjOzq7Nze3GxqbCQmJKSipPz+/BQSFJSSlP///wAAAAAAAAAAAAAAAAAAAAAA
                AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAU1oCSOZGmeZsIcKBkBgNGKCgITs5TATC4NMIGvAED4JA0A
                5AgELHwBmCynKBQUx8fjyO16RyEAIfkECQMAFAAsAAAAABAAEACEBAIEjIqMREZE3N7cFBYUZGZk
                pKak/Pr8fHp8DA4MlJaUJCIkBAYEjI6MXF5c7OrsHBocbG5s/P78fH58////AAAAAAAAAAAAAAAA
                AAAAAAAAAAAAAAAAAAAAAAAAAAAABTMgJY5kSSqQY5oFADDSOjYuMMnik7gGLgauiE8kcA2GlAWA
                gEwCIE0EINCkHKrYrHZbDQEAOy9Dd084UWJGMW9IVkV6SUFNdE1pZVB6M0RZRmM4VGc4SnNjNU5J
                NndXUWJ0S3djallXMldCVkZweUFsV2Q3QkU="
            />
            )}
            <Link to="/register" className="btn btn-link">Register</Link>
          </div>
        </form>
      </div>
    );
  }
}

function mapState(state) {
  const { loggingIn } = state.authentication;
  return { loggingIn };
}

const actionCreators = {
  login: userActions.login,
  logout: userActions.logout,
};

const connectedLoginPage = connect(mapState, actionCreators)(LoginPage);
export { connectedLoginPage as LoginPage };
