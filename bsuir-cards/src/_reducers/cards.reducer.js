import { cardsConstants } from '../_constants';

const initialState = {
  loading: false,
  cardsItems: [],
};

export function cards(state = initialState, action) {
  switch (action.type) {
    case cardsConstants.GET_CARDS_REQUEST:
      return {
        ...state, loading: true,
      };
    case cardsConstants.GET_CARDS_REQUEST_SUCCESS:
      return {
        ...state, cardsItems: action.cards,
      };
    case cardsConstants.ISSUE_CARD_REQUEST_SUCCESS:
      return state;
    case cardsConstants.ADD_NEW_CARD_REQUEST_SUCCESS:
      return state;
    case cardsConstants.DELETE_CARD_REQUEST_SUCCESS:
      return state;
    case cardsConstants.REMOVE_CARD_REQUEST_SUCCESS:
      return state;
    default:
      return state;
  }
}

export default cards;
