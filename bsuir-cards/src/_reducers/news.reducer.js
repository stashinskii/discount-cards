import { newsConstants } from '../_constants';

const initialState = {
  loading: false,
  newsItems: [],
};

export function news(state = initialState, action) {
  switch (action.type) {
    case newsConstants.GET_NEWS_REQUEST:
      return { ...state, loading: true };
    case newsConstants.GET_NEWS_REQUEST_SUCCESS:
      return { ...state, newsItems: action.news };
    default:
      return state;
  }
}

export default news;
