import { combineReducers } from 'redux';

import { authentication } from './authentication.reducer';
import { registration } from './registration.reducer';
import { users } from './users.reducer';
import { alert } from './alert.reducer';
import { news } from './news.reducer';
import { cards } from './cards.reducer';
import { shops } from './shops.reducer';


const rootReducer = combineReducers({
  authentication,
  registration,
  users,
  alert,
  news,
  cards,
  shops
});

export default rootReducer;