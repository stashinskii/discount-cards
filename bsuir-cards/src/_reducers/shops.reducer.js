import { shopsConstants } from '../_constants';

const initialState = {
  shopsItems: [],
};

export function shops(state = initialState, action) {
  switch (action.type) {
    case shopsConstants.GET_SHOPS_REQUEST_SUCCESS:
      return {
        ...state, shopsItems: action.shops,
      };
    default:
      return state;
  }
}

export default shops;
