import {
  Modal, Select, Form, Input, InputNumber,
} from 'antd';
import React from 'react';
import { shopsService } from '../../_services';

const EditCardForm = Form.create({ name: 'form_in_modal' })(
  // eslint-disable-next-line
  class extends React.Component {
    componentDidMount() {
      this.getShops();
    }

    getShops() {
      shopsService.getShops()
        .then(
          (shops) => {
            this.shops = shops;
          },
          (error) => {
            console.log('Error n news action');
          },
        );
    }

    render() {
      const {
        visible, onCancel, onCreate, form, card,
      } = this.props;
      const { getFieldDecorator } = form;
      if (!card) { return null; }
      return (
        <Modal
          visible={visible}
          title="Update card"
          okText="Update"
          onCancel={onCancel}
          onOk={onCreate}
        >
          <Form layout="vertical">
            <Form.Item label="Discount percent">
              {getFieldDecorator('discountPercent', {
                initialValue: card.discountPercent,
                rules: [{ message: 'Please input discount percent!', required: true }],
              })(<InputNumber min={0} max={100} />)}
            </Form.Item>
            <Form.Item label="Description">
              {getFieldDecorator('discountInfo', {
                rules: [{ required: true, message: 'Please input the description!' }],
                initialValue: card.discountInfo,
              })(<Input />)}
            </Form.Item>
          </Form>
        </Modal>
      );
    }
  },
);

export default EditCardForm;
