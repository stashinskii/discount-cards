import React from 'react';
import { connect } from 'react-redux';
import { Button, Modal, Switch } from 'antd';
import 'antd/dist/antd.css';

import { cardsActions } from '../_actions';
import './CardsPage.css';
import CollectionCreateForm from './CollectionCreateForm/CollectionCreateForm';
import EditCardForm from './EditCardForm/EditCardForm';
import { cardsService } from '../_services';

class CardsPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      visibleAddNewCard: false,
      visibleInfo: false,
      editCardModalVisible: false,
      currentCardInfo: null,
      currentCard: null,
      showOnlyIssuedCards: false,
      signatureLoading: false,
      signatureCorrect: false,
      signatureCheckRequested: false,
    };
  }

  componentDidMount() {
    const { getAll } = this.props;
    getAll();
  }


  handleIssueCardClick = () => {
    this.props.issueCard(event.srcElement.id);
  }

  showAddNewCardModal = () => {
    this.setState({ visibleAddNewCard: true });
  };


  deleteCardAdmin = (cardId) => {
    const { deleteCardAdmin } = this.props;
    deleteCardAdmin(cardId);
  }


  removeCard = () => {
    const id = event.srcElement.id.replace('remove-', '');
    this.props.removeCard(id);
  }

  showCardInfoModal = () => {
    const id = event.srcElement.id.replace('info-', '');
    const currentCard = this.props.cards.cardsItems.find((e) => e.discountCardsId == id);
    this.setState({ visibleInfo: true });
    this.setState({ currentCardInfo: currentCard });


    const requestOptions = {
      method: 'GET',
    };
    let outside;
    fetch(`https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=${currentCard.digitalSignature}`, requestOptions)
      .then((response) => response.blob())
      .then((data) => {
        outside = URL.createObjectURL(data);
        this.setState({ cardQrCode: outside });
      });
    // alert(this.state.cardQrCode)
  }

  showUpdateCardModal = (card) => {
    this.setState({
      editCardModalVisible: true,
      currentCard: card,
    });
  }

  handleCancelAddNewCard = () => {
    this.setState({ visibleAddNewCard: false });
  };

  handleCreateAddNewCard = () => {
    const { form } = this.formRef.props;
    const { addNewCard } = this.props;
    form.validateFields((err, values) => {
      if (err) {
        return;
      }
      addNewCard(JSON.stringify(values));
      form.resetFields();
      this.setState({ visibleAddNewCard: false });
    });
  };

  handleCandelUpdateCard = () => {
    this.setState({ editCardModalVisible: false, currentCard: null });
  }

  handleUpdateCard = () => {
    const { form } = this.updateCardFormRef.props;
    const { updateCard } = this.props;
    const { currentCard } = this.state;
    form.validateFields((err, values) => {
      if (err) {
        return;
      }

      const card = { discountCardId: currentCard.discountCardsId, ...values };
      updateCard(JSON.stringify(card));
      form.resetFields();
      this.setState({ editCardModalVisible: false });
    });
  }

  saveFormRef = (formRef) => {
    this.formRef = formRef;
  };

  toggleVisibleCards = () => {
    this.setState((prevState) => ({
      showOnlyIssuedCards: !prevState.showOnlyIssuedCards,
    }));
  }

  checkSignature = (card) => {
    const { signatureLoading } = this.state;
    const { user } = this.props;
    if (signatureLoading) { return; }
    this.setState({
      signatureCheckRequested: true,
      signatureLoading: true,
    });

    const params = {
      cardId: card.discountCardsId,
      signature: card.digitalSignature,
      userId: user.id,
    };

    const self = this;

    cardsService.verifySignature(JSON.stringify(params)).then(() => {
      self.setState({
        signatureCorrect: true,
        signatureLoading: false,
      });
    });
  }


  render() {
    const { cards: { cardsItems }, user } = this.props;

    const {
      visibleAddNewCard, visibleInfo, currentCardInfo, cardQrCode,
      editCardModalVisible, currentCard, showOnlyIssuedCards,
      signatureLoading, signatureCorrect, signatureCheckRequested,
    } = this.state;

    let visibleCards = cardsItems;

    if (showOnlyIssuedCards) {
      visibleCards = cardsItems.filter((card) => card.issuedForUser === true);
    }

    return (
      <div>
        {user && user.isAdmin && (
          <div className="admin-panel">
            <p>
              🔓 Admin panel (
              {`${user.firstName} ${user.lastName}`}
              )
            </p>
            <Button type="success" className="admin-create-card-form" onClick={this.showAddNewCardModal}>
              Create new card 🔑
            </Button>
            <CollectionCreateForm
              wrappedComponentRef={this.saveFormRef}
              visible={visibleAddNewCard}
              onCancel={this.handleCancelAddNewCard}
              onCreate={this.handleCreateAddNewCard}
            />
            <EditCardForm
              wrappedComponentRef={(ref) => { this.updateCardFormRef = ref; }}
              visible={editCardModalVisible}
              onCancel={this.handleCandelUpdateCard}
              onCreate={this.handleUpdateCard}
              card={currentCard}
            />
          </div>
        )}

        {user && (
          <div className="flex-row mb-2">
            <span className="mr-2">Show only issued cards:</span>
            <Switch checked={showOnlyIssuedCards} onChange={this.toggleVisibleCards} />
          </div>
        )}


        <Modal
          title="Card info"
          visible={visibleInfo}
          onOk={() => this.setState({ visibleInfo: false, signatureCheckRequested: false })}
          onCancel={() => this.setState({ visibleInfo: false, signatureCheckRequested: false })}
          state={this.state}
        >
          <h4>{currentCardInfo && currentCardInfo.shop.shopName}</h4>
          <p>
            <b>Discount percent:</b>
            {' '}
            {currentCardInfo && currentCardInfo.discountPercent}
            %
          </p>
          <p>
            <b>Info:</b>
            {' '}
            {currentCardInfo && currentCardInfo.discountInfo}
          </p>
          {user && (
            <div>
              <p>
                <b>Digital signature:</b>
                {' '}
                {currentCardInfo && currentCardInfo.digitalSignature}
              </p>
              {cardQrCode && (
                <img src={cardQrCode} alt="qr code" />
              )}
            </div>
          )}
          {user && user.isAdmin && (
            <>

              <p className="mt-2">
                <Button type="success" className="mr-2" onClick={() => { this.checkSignature(currentCardInfo); }}>
                  Check signature 🔑
                </Button>
                {signatureCheckRequested && (
                  <>
                    {signatureLoading && (
                      <span>Signature check is in progress</span>
                    )}
                    {!signatureLoading && (
                      <>
                        {signatureCorrect && (
                          <span>Signature is correct</span>
                        )}
                        {!signatureCorrect && (
                          <span>Signature is wrong</span>
                        )}
                      </>
                    )}
                  </>
                )}
              </p>
            </>
          )}
        </Modal>


        <div className="grid-listing">
          {visibleCards.map((card) => (
            <div className="grid__item" key={card.discountCardsId}>
              <div className="card">
                <img className="card-image" src={card.shop.imageUrl ? card.shop.imageUrl : 'https://www.creativefabrica.com/wp-content/uploads/2018/08/Sale-Discount-Shoppping-Shop-Logo-by-Mansel-Brist-2.jpg'} alt="Avatar" />
                <div className="card-container">
                  {user && (
                    <a onClick={this.handleIssueCardClick} id={card.discountCardsId} className={card.issuedForUser ? 'btn btn-outline disabled' : 'btn btn-outline'}>Issue card</a>
                  )}
                  {' '}
                  {user && card.issuedForUser && (
                    <a id={`remove-${card.discountCardsId}`} onClick={this.removeCard} className="btn btn-dark">❌</a>
                  )}
                  {' '}
                  <span><a id={`info-${card.discountCardsId}`} onClick={this.showCardInfoModal} className="btn btn-dark">Info</a></span>
                  {' '}
                  {user && user.isAdmin && (
                    <span><a onClick={() => this.showUpdateCardModal(card)} className="btn btn-dark">Update card 🔑</a></span>
                  )}
                  {' '}
                  {user && user.isAdmin && (
                    <span><a onClick={() => this.deleteCardAdmin(card.discountCardsId)} className="btn btn-dark">Delete card 🔑</a></span>
                  )}

                  <h4 className="card-header">
                    <b>
                      {card.shop.shopName}
                      {' '}
                    </b>
                    <span className="label label-info">
                      {card.discountPercent}
                      %
                    </span>
                  </h4>
                  <p>{card.discountInfo}</p>

                </div>
              </div>
            </div>
          ))}

        </div>
      </div>

    );
  }
}

function mapState(state) {
  const { cards, authentication } = state;
  const { user } = authentication;
  return { cards, user };
}

const actionCreators = {
  getAll: cardsActions.getAll,
  issueCard: cardsActions.issueCard,
  addNewCard: cardsActions.addNewCard,
  deleteCardAdmin: cardsActions.deleteCardAdmin,
  removeCard: cardsActions.removeCard,
  updateCard: cardsActions.updateCard,
};

const ConnectedCardsPage = connect(mapState, actionCreators)(CardsPage);

export { ConnectedCardsPage as CardsPage };
