import {
  Modal, Select, Form, Input, InputNumber,
} from 'antd';
import React from 'react';
import { connect } from 'react-redux';
import { shopsService } from '../../_services';

const CollectionCreateForm = Form.create({ name: 'form_in_modal' })(
  // eslint-disable-next-line
  class extends React.Component {

    componentDidMount() {
      this.getShops();
    }

    getShops() {
      shopsService.getShops()
        .then(
          (shops) => {
            this.shops = shops;
          },
          (error) => {
            console.log('Error n news action');
          },
        );
    }

    render() {
      const {
        visible, onCancel, onCreate, form,
      } = this.props;
      const { getFieldDecorator } = form;
      return (
        <Modal
          visible={visible}
          title="Add new card"
          okText="Create"
          onCancel={onCancel}
          onOk={onCreate}
        >
          <Form layout="vertical">
            <Form.Item label="Discount percent">
              {getFieldDecorator('discountPercent', {
                rules: [{ message: 'Please input the discount percent!', required: true }],
              })(<InputNumber min={0} max={100} />)}
            </Form.Item>
            <Form.Item label="Description">
              {getFieldDecorator('discountInfo', {
                rules: [{ message: 'Please input the description!', required: true }],
              })(<Input />)}
            </Form.Item>

            <Form.Item label="Select" hasFeedback>
              {getFieldDecorator('shopId', {
                rules: [{ required: true, message: 'Please select a shop!' }],
              })(
                <Select placeholder="Please select a shop">
                  {this.shops && this.shops.map((shop) => (
                    <Select.Option key={shop.shopId} value={shop.shopId}>{shop.shopName}</Select.Option>
                  ))}
                </Select>,
              )}
            </Form.Item>

          </Form>
        </Modal>
      );
    }
  },
);

export default CollectionCreateForm;
