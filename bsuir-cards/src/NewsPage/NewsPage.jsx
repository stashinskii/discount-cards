import React from 'react';
import { connect } from 'react-redux';
import {
  Button, Modal, Form, InputNumber,
} from 'antd';
import 'antd/dist/antd.css';
import { newsActions } from '../_actions';
import './NewsPage.sass';
import AddArticleModal from './AddArticleModal/AddArticleModal';

class NewsPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      formVisible: false,
    };
  }

  componentDidMount() {
    const { getAll } = this.props;
    getAll();
  }

  handleFormCancel = () => {
    this.setState({ formVisible: false });
  };

  handleSubmit = (article) => {
    const { addArticle } = this.props;
    addArticle(JSON.stringify(article));
    this.setState({ formVisible: false });
  };

  openForm = () => {
    this.setState({
      formVisible: true,
    });
  }

  render() {
    const { news, user } = this.props;
    const { formVisible } = this.state;

    return (
      <div className="news">
        {user && user.isAdmin && (
          <>
            <Button type="success" className="admin-create-card-form" onClick={this.openForm}>
              Add article
            </Button>
            <AddArticleModal
              wrappedComponentRef={this.saveFormRef}
              visible={formVisible}
              onCancel={this.handleFormCancel}
              onCreate={this.handleSubmit}
            />
          </>
        )}
        {news.map((article) => (
          <div className="card article" key={article.articleId}>
            <div className="card-body">
              <h5 className="card-title">
                {article.title}
              </h5>
              <div className="card-text">
                {article.text}
              </div>
              <div className="category">
                <span className="label label-info">
                {article.category}
                    </span>
                
              </div>
              {user && user.isAdmin && (
                <span>
                  <a
                    onClick={() => this.props.deleteArticle(article.articleId)}
                    className="btn btn-dark"
                  >
                    Delete article 🔑
                  </a>
                </span>
              )}
            </div>
          </div>
        ))}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  news: state.news.newsItems,
  user: state.authentication.user,
});

const mapDispatchToProps = (dispatch) => ({
  getAll: () => dispatch(newsActions.getAll()),
  addArticle: (article) => dispatch(newsActions.addArticle(article)),
  deleteArticle: (articleId) => dispatch(newsActions.deleteArticle(articleId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(NewsPage);
