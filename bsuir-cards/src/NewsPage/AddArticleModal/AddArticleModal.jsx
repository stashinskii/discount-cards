import {
  Modal, Form, Input,
} from 'antd';
import React from 'react';

const AddArticleModal = Form.create({ name: 'add-article-modal' })(
  // eslint-disable-next-line
  class extends React.Component {
    onSubmit = () => {
      const { onCreate, form: { validateFields } } = this.props;
      validateFields((err, values) => {
        if (err) { return; }
        onCreate(values);
      });
    }

    render() {
      const {
        visible, onCancel, onCreate, form,
      } = this.props;
      const { getFieldDecorator } = form;
      return (
        <Modal
          visible={visible}
          title="Add new article"
          okText="Create"
          onCancel={onCancel}
          onOk={this.onSubmit}
        >
          <Form layout="vertical">
            <Form.Item label="Title">
              {getFieldDecorator('title', {
                rules: [{ required: true, message: 'Please input title!' }],
              })(<Input />)}
            </Form.Item>
            <Form.Item label="Description">
              {getFieldDecorator('text', {
                rules: [{ required: true, message: 'Please input description!' }],
              })(<Input />)}
            </Form.Item>
            <Form.Item label="Category">
              {getFieldDecorator('category', {
                rules: [{ required: true, message: 'Please input category!' }],
              })(<Input />)}
            </Form.Item>
          </Form>
        </Modal>
      );
    }
  },
);

export default AddArticleModal;
