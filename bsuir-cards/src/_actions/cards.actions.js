import { cardsConstants } from '../_constants';
import { cardsService } from '../_services';
import { alertActions } from '.';

function getAll() {
  return (dispatch) => {
    dispatch(request());

    cardsService.getCards()
      .then(
        (cards) => {
          dispatch(success(cards));
        },
        (error) => {
          console.log('Error n news action');
        },
      );
  };

  function request() { return { type: cardsConstants.GET_CARDS_REQUEST }; }
  function success(cards) { return { type: cardsConstants.GET_CARDS_REQUEST_SUCCESS, cards }; }
}

function issueCard(cardId) {
  return (dispatch) => {
    cardsService.issueCard(cardId)
      .then(() => {
        dispatch(alertActions.success('Card have been issued'));
        getAll()(dispatch);
      },
      (error) => {
        dispatch(alertActions.error(error.toString()));
      });
  };
}

function addNewCard(card) {
  return (dispatch) => {
    cardsService.addNewCard(card)
      .then(() => {
        dispatch(alertActions.success('Card have been creted by admin'));
        getAll()(dispatch);
      },
      (error) => {
        dispatch(alertActions.error(error.toString()));
      });
  };
}

function updateCard(card) {
  return (dispatch) => {
    cardsService.updateCard(card)
      .then(() => {
        dispatch(alertActions.success('Card have been updated by admin'));
        getAll()(dispatch);
      },
      (error) => {
        dispatch(alertActions.error(error.toString()));
      });
  };
}

function deleteCardAdmin(cardId) {
  return (dispatch) => {
    cardsService.deleteCardAdmin(cardId)
      .then(() => {
        dispatch(alertActions.success(`Card (${cardId}) were deleted`));
        getAll()(dispatch);
      },
      (error) => {
        dispatch(alertActions.error(error.toString()));
      });
  };
}

function removeCard(cardId) {
  return (dispatch) => {
    cardsService.removeCard(cardId)
      .then(() => {
        dispatch(alertActions.success(`Card (${cardId}) were removed`));
        getAll()(dispatch);
      },
      (error) => {
        dispatch(alertActions.error(error.toString()));
      });
  };
}


export const cardsActions = {
  getAll,
  issueCard,
  addNewCard,
  deleteCardAdmin,
  removeCard,
  updateCard,
};

export default cardsActions;
