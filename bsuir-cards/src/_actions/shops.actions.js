import { shopsConstants } from '../_constants';
import { shopsService } from '../_services';
import { alertActions } from './alert.actions';

function success(shops) { return { type: shopsConstants.GET_SHOPS_REQUEST_SUCCESS, shops }; }

function getAllShops() {
  return (dispatch) => {
    shopsService.getShops()
      .then(
        (shops) => {
          dispatch(success(shops));
        },
        () => {
          console.log('Error n shops action');
        },
      );
  };
}

function addShop(shop) {
  return (dispatch) => {
    shopsService.addShop(shop).then(() => {
      dispatch(alertActions.success('Shop have been added'));
      getAllShops()(dispatch);
    });
  };
}

function deleteShop(shopId) {
  return (dispatch) => {
    shopsService.deleteShop(shopId)
      .then(() => {
        dispatch(alertActions.success(`Shop (${shopId}) was deleted`));
        getAllShops()(dispatch);
      },
      (error) => {
        dispatch(alertActions.error(error.toString()));
      });
  };
}


export const shopsActions = {
  getAllShops,
  addShop,
  deleteShop,
};

export default shopsActions;
