import { newsConstants } from '../_constants';
import { newsService } from '../_services';
import { alertActions } from './alert.actions';

function request() { return { type: newsConstants.GET_NEWS_REQUEST }; }
function success(news) { return { type: newsConstants.GET_NEWS_REQUEST_SUCCESS, news }; }

function getAll() {
  return (dispatch) => {
    dispatch(request());

    newsService.getNews()
      .then(
        (news) => {
          dispatch(success(news));
        },
        (error) => {
          console.log(error);
          console.log('Error n news action');
        },
      );
  };
}

function addArticle(article) {
  return (dispatch) => {
    newsService.addArticle(article).then(() => {
      dispatch(alertActions.success('Article have been added'));
      getAll()(dispatch);
    });
  };
}


function deleteArticle(articleId) {
  return (dispatch) => {
    newsService.deleteArticle(articleId)
      .then(() => {
        dispatch(alertActions.success(`Article (${articleId}) was deleted`));
        getAll()(dispatch);
      },
      (error) => {
        dispatch(alertActions.error(error.toString()));
      });
  };
}


export const newsActions = {
  getAll,
  addArticle,
  deleteArticle
};


export default newsActions;
